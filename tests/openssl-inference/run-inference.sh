#!/bin/sh

set -e

mkdir -p output/inferer
../../src/infer_server.py -R 127.0.0.1:44433 --timeout 0.5 -E BDist:2 -v -S tls13 -o output/inferer
