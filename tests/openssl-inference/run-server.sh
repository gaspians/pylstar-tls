#!/bin/bash

set -e

PID_FILE=output/server/server.pid

mkdir -p output/server

if [ -f "$PID_FILE" ]; then
	echo "A server seems to be already running"
	exit 1
fi

openssl req -newkey rsa:2048 -x509 -out output/server/cert.pem -keyout output/server/key.pem -nodes -config openssl.cnf
openssl s_server -debug -msg -accept 127.0.0.1:44433 -cert output/server/cert.pem -key output/server/key.pem -www > output/server/server.out 2> output/server/server.err &
echo $! > "$PID_FILE"
