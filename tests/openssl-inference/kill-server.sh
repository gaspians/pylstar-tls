#!/bin/bash

set -e

PID_FILE=output/server/server.pid

if [ ! -f "$PID_FILE" ]; then
	echo "No server seems to be running"
	exit 1
fi

echo "Killing Server ($(cat "$PID_FILE"))"
kill "$(cat "$PID_FILE")"
rm -f "$PID_FILE"
